package az.card.mscard.model.constants;

public interface Headers {
    String AUTHORIZATION = "Authorization";
    String LANGUAGE = "Accept-Language";
}
