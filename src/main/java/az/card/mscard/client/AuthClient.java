package az.card.mscard.client;

import az.card.mscard.model.request.VerifyTokenRequest;
import az.card.mscard.model.response.AuthPayloadResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(
        name = "ms-auth",
        url = "localhost:8001/"
)
public interface AuthClient {

    @PostMapping("/api/auth/verify")
    AuthPayloadResponse verifyAccessToken(@RequestBody VerifyTokenRequest request);
}
