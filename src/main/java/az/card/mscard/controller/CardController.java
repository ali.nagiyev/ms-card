package az.card.mscard.controller;

import az.card.mscard.model.dto.CardDto;
import az.card.mscard.model.request.CardRequest;
import az.card.mscard.service.CardService;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static az.card.mscard.model.constants.Headers.AUTHORIZATION;
import static lombok.AccessLevel.PRIVATE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/cards")
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class CardController {

    CardService cardService;

    @GetMapping
    public List<CardDto> getUserAllAvailableCards(
            @RequestHeader(value = AUTHORIZATION) String accessToken,
            @PageableDefault Pageable pageable
    ) {
        return cardService.getUserAllCards(pageable, accessToken);
    }

    @GetMapping("/uuid")
    public Map<String, String> getProductByUuid(
            @RequestParam List<String> uuid
    ){
        return cardService.getCardNumbersByUUID(uuid);
    }

    @PostMapping
    public void saveCard(
            @RequestHeader(value = AUTHORIZATION) String accessToken,
            @RequestBody CardRequest cardRequest
    ) {
        cardService.saveCard(cardRequest, accessToken);
    }

    @PutMapping
    public void checkAndSubtractAmount(
            @RequestParam String cardUuid,
            @RequestParam BigDecimal amount
    ) {
        cardService.checkAndSubtractAmount(cardUuid, amount);
    }

    @PutMapping("/reverse")
    public void reverseBalance(
            @RequestParam String cardUuid,
            @RequestParam BigDecimal amount
    ) {
        cardService.reverseBalance(cardUuid, amount);
    }
}
