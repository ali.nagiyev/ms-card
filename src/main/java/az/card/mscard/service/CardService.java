package az.card.mscard.service;

import az.card.mscard.client.AuthClient;
import az.card.mscard.dao.entity.CardEntity;
import az.card.mscard.dao.repository.CardRepository;
import az.card.mscard.exception.BadRequestException;
import az.card.mscard.mapper.CardMapper;
import az.card.mscard.model.dto.CardDto;
import az.card.mscard.model.request.CardRequest;
import az.card.mscard.model.request.VerifyTokenRequest;
import az.card.mscard.util.EncryptionUtil;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static az.card.mscard.util.EncryptionUtil.decrypt;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static lombok.AccessLevel.PRIVATE;


@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class CardService {

    CardRepository cardRepository;
    EncryptionUtil encryptionUtil;
    CardMapper cardMapper;
    AuthClient authClient;

    public List<CardDto> getUserAllCards(Pageable pageable, String accessToken) {

        var userUuid = authClient.verifyAccessToken(VerifyTokenRequest.of(accessToken)).getUserUUID();

        return cardRepository.findAllByUserUuid(userUuid, pageable)
                .stream()
                .map(cardMapper::toCardDto)
                .collect(toList());
    }

    public Map<String, String> getCardNumbersByUUID(List<String> cardUUIDs) {

        var cardEntities = cardRepository.findAllByCardUuidIn(cardUUIDs);

        return cardEntities.stream()
                .collect(
                        toMap(
                                CardEntity::getCardUuid, t -> decrypt(t.getEncryptedCardNumber())
                        )
                );
    }

    public void saveCard(CardRequest cardRequest, String accessToken) {

        var userUuid = authClient.verifyAccessToken(VerifyTokenRequest.of(accessToken)).getUserUUID();

        cardRepository.save(
                CardEntity.builder()
                        .encryptedCardNumber(encryptionUtil.encrypt(cardRequest.getCardNumber()))
                        .expirationDate(encryptionUtil.encrypt(cardRequest.getExpiredDate()))
                        .cvv(encryptionUtil.encrypt(cardRequest.getCvv()))
                        .cardUuid(UUID.randomUUID().toString())
                        .userUuid(userUuid)
                        .balance(BigDecimal.TEN)
                        .build()
        );
    }

    public void checkAndSubtractAmount(String cardUuid, BigDecimal amount) {

        var cardEntity = cardRepository.findByCardUuid(cardUuid)
                .orElseThrow(
                        () -> new BadRequestException("There is no card with this uuid", "GENERAL_EXCEPTION")
                );

        if (cardEntity.getBalance().doubleValue() < amount.doubleValue())
            throw new BadRequestException("There is insufficient balance", "GENERAL_EXCEPTION");

        cardEntity.setBalance(cardEntity.getBalance().subtract(amount));

        cardRepository.save(cardEntity);
    }

    public void reverseBalance(String cardUuid, BigDecimal amount) {

        var cardEntity = cardRepository.findByCardUuid(cardUuid)
                .orElseThrow(
                        () -> new BadRequestException("There is no card with this uuid", "GENERAL_EXCEPTION")
                );

        cardEntity.setBalance(cardEntity.getBalance().add(amount));

        cardRepository.save(cardEntity);
    }
}
